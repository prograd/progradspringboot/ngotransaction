package com.example.BankSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BankSpringBootApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext context=SpringApplication.run(BankSpringBootApplication.class, args);
        PersonBank personBank=context.getBean(PersonBank.class);
		personBank.accept();

		Nitu nitu=context.getBean(Nitu.class);
		nitu.calculatInterest();
		nitu.showInterest();

	}

}
